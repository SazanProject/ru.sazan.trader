﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Emulation
{
    public class AlwaysTimeToTradeSchedule:TradingSchedule
    {
        public bool ItIsTimeToTrade(DateTime dateTime)
        {
            return true;
        }

        public DateTime SessionEnd
        {
            get
            {
                return DateTime.MaxValue;
            }
        }
    }

    public class NeverTimeToTradeSchedule : TradingSchedule
    {
        public bool ItIsTimeToTrade(DateTime dateTime)
        {
            return false;
        }

        public DateTime SessionEnd
        {
            get
            {
                return DateTime.MinValue;
            }
        }
    }
}
