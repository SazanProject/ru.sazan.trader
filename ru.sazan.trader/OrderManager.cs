﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;

namespace ru.sazan.trader
{
    public interface OrderManager
    {
        void PlaceOrder(Order order);
        void MoveOrder(Order order, double price);
        void CancelOrder(Order order);
    }
}
