﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.Collections
{
    public class ObservableCollection<T>:List<T>,ItemHasBeenAddedNotifier<T>
    {
        private List<GenericObserver<T>> observers;

        public ObservableCollection()
        {
            this.observers = new List<GenericObserver<T>>();
        }

        public void NotifyObservers(T item)
        {
            foreach (GenericObserver<T> observer in this.observers)
                observer.Update(item);
        }

        public void RegisterObserver(GenericObserver<T> observer)
        {
            this.observers.Add(observer);
        }

        public new void Add(T item)
        {
            base.Add(item);
            this.NotifyObservers(item);

            if (this.OnItemAdded != null)
                this.OnItemAdded(item);
        }

        public event ItemHasBeenAddedNotification<T> OnItemAdded;
    }
}
