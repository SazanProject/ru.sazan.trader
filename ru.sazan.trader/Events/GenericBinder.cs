﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Events
{
    public interface GenericBinder
    {
        void Bind<T>(T method);
        void Unbind<T>(T method);
    }
}
