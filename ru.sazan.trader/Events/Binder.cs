﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Events
{
    public interface Binder
    {
        void Bind();
        void Unbind();
        int BindedHandlersCounter { get; }
    }
}
