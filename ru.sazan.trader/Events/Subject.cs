﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Events
{
    public class Subject
    {
        private List<Observer> observers;

        public Subject()
        {
            this.observers = new List<Observer>();
        }

        public void NotifyObservers()
        {
            foreach (Observer observer in this.observers)
                observer.Update();
        }

        public void RegisterObserver(Observer observer)
        {
            this.observers.Add(observer);
        }
    }
}
