using System;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.Net
{
	public class InServiceMessage : Message
	{
		private Guid sourceAddress;
		private Guid destinationAddress;
		private InServiceMessage(){}

		public InServiceMessage(Guid srcAddr, Guid dstAddr)
		{
			this.sourceAddress = srcAddr;
			this.destinationAddress = dstAddr;
		}

		public byte[] GetSourceAddressBytes()
		{
			return this.sourceAddress.ToByteArray();
		}

		public byte[] GetDestinationAddressBytes()
		{
			return this.destinationAddress.ToByteArray();
		}

		public byte[] GetContentBytes()
		{
			string content = "in service";

			return content.AsByteArray();
		}
	}
}
