﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Extensions
{
    public static class ListExtensions
    {
        public static bool ItemsAreOlderThanSeconds<T>(this List<T> items, int seconds)
            where T:TimeStamped
        {
            if (items.Count == 0)
                return false;

            return BrokerDateTime.Make(DateTime.Now).AddSeconds(- seconds) > items.Last().DateTime;
        }
    }
}
