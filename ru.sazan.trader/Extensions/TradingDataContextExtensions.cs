﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Extensions
{
    public static class TradingDataContextExtensions
    {
        public static IEnumerable<Order> GetOpenOrders(this DataContext context, Strategy strategy)
        {
            try
            {
                return from s in context.Get<IEnumerable<OpenOrder>>()
                            where s.Order.Signal.StrategyId == strategy.Id 
                            select s.Order;
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Order> GetFilledOpenOrders(this DataContext context, Strategy strategy)
        {
            return context.GetOpenOrders(strategy).Where(o => o.IsFilled || o.IsFilledPartially);
        }

        public static IEnumerable<Order> GetCloseOrders(this DataContext context, Strategy strategy)
        {
            try
            {
                return from s in context.Get<IEnumerable<CloseOrder>>()
                       where s.Order.Signal.StrategyId == strategy.Id
                       select s.Order;
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Order> GetFilledCloseOrders(this DataContext context, Strategy strategy)
        {
            return context.GetCloseOrders(strategy).Where(o => o.IsFilled || o.IsFilledPartially);
        }

        public static IEnumerable<Trade> GetTrades(this DataContext context, Strategy strategy)
        {
            try
            {
                return from s in context.Get<IEnumerable<Strategy>>()
                       join signal in context.Get<IEnumerable<Signal>>()
                       on s equals signal.Strategy
                       join order in context.Get<IEnumerable<Order>>()
                       on signal equals order.Signal
                       join trade in context.Get<IEnumerable<Trade>>()
                       on order equals trade.Order
                       where s.Id == strategy.Id
                       select trade;
            }
            catch
            {
                return null;
            }
        }

        public static double GetAmount(this DataContext context, Strategy strategy)
        {
           return context.GetTrades(strategy).Sum(t => t.Amount);
        }

        public static IEnumerable<Trade> GetTrades(this DataContext context, Order order)
        {
            try
            {
                return from t in context.Get<IEnumerable<Trade>>()
                       where t.OrderId == order.Id
                       select t;
            }
            catch
            {
                return null;
            }
        }

        public static bool PositionExists(this DataContext context, Strategy strategy)
        {
            return context.GetAmount(strategy) != 0;
        }

        public static IEnumerable<Trade> GetBuyTrades(this DataContext context, Strategy strategy)
        {
            return context.GetTrades(strategy).Where(t => t.Amount > 0);
        }

        public static IEnumerable<Trade> GetSellTrades(this DataContext context, Strategy strategy)
        {
            return context.GetTrades(strategy).Where(t => t.Amount < 0);
        }

        /// <summary>
        /// Вычислить значение реализованной прибыли или убытка в пунктах для стратегии.
        /// </summary>
        /// <param name="context">Экземпляр контекста торговых данных.</param>
        /// <param name="strategy">Экземпляр описания стратегии, для которой необходимо получить результат.</param>
        /// <returns>Возвращает значение реализованной прибыли или убытка только для закрытых позиций, без учета комиссии.</returns>
        public static double GetProfitAndLossPoints(this DataContext context, Strategy strategy)
        {
            IEnumerable<Trade> buyTrades = context.GetBuyTrades(strategy);
            IEnumerable<Trade> sellTrades = context.GetSellTrades(strategy);

            double buyAmount = Math.Abs(buyTrades.Sum(t => t.Amount));
            double sellAmount = Math.Abs(sellTrades.Sum(t => t.Amount));

            if (buyAmount > sellAmount)
                return GetPointsSum(sellTrades) - GetPointsSum(buyTrades.TakeForAmount(sellAmount));
            
            if (sellAmount > buyAmount)
                return GetPointsSum(sellTrades.TakeForAmount(buyAmount)) - GetPointsSum(buyTrades);

            return GetPointsSum(sellTrades) - GetPointsSum(buyTrades);
        }

        private static double GetPointsSum(IEnumerable<Trade> trades)
        {
            return trades.Sum(t => (Math.Abs(t.Amount) * t.Price));
        }

        public static double GetProfitAndLoss(this DataContext context, Strategy strategy)
        {
            Symbol symbol = GetSymbol(context, strategy);

            if (symbol == null)
                return GetProfitAndLossPoints(context, strategy);

            return GetProfitAndLossPoints(context, strategy) / symbol.Step * symbol.StepPrice;
        }

        public static Symbol GetSymbol(this DataContext context, Strategy strategy)
        {
            try
            {
                return context.Get<IEnumerable<Symbol>>().Single(s => s.Name == strategy.Symbol);
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Order> GetUnfilled(this DataContext context, Strategy strategy, OrderType type)
        {
            try
            {
                return context.Get<IEnumerable<Order>>().Where(o => o.Signal.StrategyId == strategy.Id
                    && o.OrderType == type
                    && !o.IsCanceled
                    && !o.IsExpired
                    && !o.IsFilled
                    && !o.IsRejected);
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Order> GetUnfilled(this DataContext context, Strategy strategy)
        {
            try
            {
                return context.Get<IEnumerable<Order>>().Where(o => o.Signal.StrategyId == strategy.Id
                    && !o.IsCanceled
                    && !o.IsExpired
                    && !o.IsFilled
                    && !o.IsRejected);
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Order> GetUndelivered(this DataContext context, Strategy strategy, OrderType type)
        {
            try
            {
                return context.Get<IEnumerable<Order>>().Where(o => o.Signal.StrategyId == strategy.Id
                    && o.OrderType == type
                    && !o.IsDelivered
                    && !o.IsExpired);
            }
            catch
            {
                return null;
            }
        }

        public static bool UndeliveredExists(this DataContext context, Strategy strategy, OrderType type)
        {
            return GetUndelivered(context, strategy, type).Count() > 0;
        }

        public static bool UnfilledExists(this DataContext context, Strategy strategy, OrderType type)
        {
            return GetUnfilled(context, strategy, type).Count() > 0;
        }

        public static bool UnfilledExists(this DataContext context, Strategy strategy)
        {
            return GetUnfilled(context, strategy).Count() > 0;
        }

        public static StopPointsSettings GetStopPointsSettings(this DataContext context, Strategy strategy)
        {
            try
            {
                return context.Get<IEnumerable<StopPointsSettings>>().Single(s => s.StrategyId == strategy.Id);
            }
            catch
            {
                return null;
            }
        }

        public static ProfitPointsSettings GetProfitPointsSettings(this DataContext context, Strategy strategy)
        {
            try
            {
                return context.Get<IEnumerable<ProfitPointsSettings>>().Single(s => s.StrategyId == strategy.Id);
            }
            catch
            {
                return null;
            }
        }

        public static Symbol GetSymbol(this DataContext context, string symbol)
        {
            try
            {
                return context.Get<IEnumerable<Symbol>>().Single(s => s.Name == symbol);
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<Trade> GetLastOpenOrderTrades(this DataContext context, Strategy strategy)
        {
            try
            {
                Order lastOpenOrder = context.GetFilledOpenOrders(strategy).Last();

                if (lastOpenOrder == null)
                    return null;

                return context.GetTrades(lastOpenOrder);
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<OrderMoveRequest> GetMoveRequests(this DataContext context, Order order)
        {
            try
            {
                return context.Get<IEnumerable<OrderMoveRequest>>().Where(r => r.OrderId == order.Id);
            }
            catch
            {
                return null;
            }
        }

        public static bool IsOpening(this DataContext context, Trade trade)
        {
            try
            {
                return context.Get<IEnumerable<OpenOrder>>().Any(o => o.OrderId == trade.OrderId);
            }
            catch
            {
                return false;
            }
        }

        public static bool IsClosing(this DataContext context, Trade trade)
        {
            try
            {
                return context.Get<IEnumerable<CloseOrder>>().Any(o => o.OrderId == trade.OrderId);
            }
            catch
            {
                return false;
            }
        }

        public static double GetCurrentProfitAndLoss(this DataContext context, Strategy strategy)
        {
            double result = 0;
            double amount = context.GetAmount(strategy);

            if (amount == 0)
                return result;

            TradeAction expectedAction = amount > 0 ? TradeAction.Buy : TradeAction.Sell;

            Trade[] trades = context.GetTrades(strategy).ToArray();

            Tick lastTick = context.Get<IEnumerable<Tick>>().LastOrDefault(t => t.Symbol == strategy.Symbol);

            if(lastTick == null)
                return 0;

            double currentPrice = lastTick.Price;

            for (int i = trades.Length - 1; i >= 0; i--)
            {
                if (trades[i].Action != expectedAction)
                    continue;

                amount -= trades[i].Amount;

                if (amount >= 0)
                {
                    result += trades[i].Amount * (currentPrice - trades[i].Price);
                }
                else 
                {
                    result += (trades[i].Amount + amount) * (currentPrice - trades[i].Price);

                    break;
                }
            }

            return result;
        }

        public static bool HasLongPosition(this DataContext context, Strategy strategy)
        {
            return context.GetAmount(strategy) > 0;
        }

        public static bool HasShortPosition(this DataContext context, Strategy strategy)
        {
            return context.GetAmount(strategy) < 0;
        }

        public static IEnumerable<Order> GetFilledPartially(this DataContext context, Strategy strategy, OrderType type)
        {
            try
            {
                return context.Get<IEnumerable<Order>>().Where(o => o.Signal.StrategyId == strategy.Id
                    && o.OrderType == type
                    && o.IsFilledPartially
                    && !o.IsCanceled
                    && !o.IsExpired
                    && !o.IsFilled
                    && !o.IsRejected);
            }
            catch
            {
                return null;
            }
        }

        public static Signal MakeSignalToClosePosition(this DataContext context, Strategy strategy)
        {
            if (!context.PositionExists(strategy))
                return null;

            if (context.HasLongPosition(strategy))
                return new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, 0, 0, 0);

            return new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 0, 0, 0);
        }

        public static Trade GetPositionOpenTrade(this DataContext context, Strategy strategy)
        {
            if (context.HasLongPosition(strategy))
                return context.Get<IEnumerable<Trade>>().LastOrDefault(s => s.Order.Signal.StrategyId == strategy.Id
                    && s.Amount > 0);

            return context.Get<IEnumerable<Trade>>().LastOrDefault(s => s.Order.Signal.StrategyId == strategy.Id
                && s.Amount < 0);
        }

        public static ICollection<Trade> GetPositionTrades(this DataContext context, Strategy strategy)
        {
            GenericFactory<ICollection<Trade>> factory =
                new PositionTradesFactory(context, strategy);

            return factory.Make();
        }

        public static double GetPositionPoints(this DataContext context, Strategy strategy)
        {
            GenericFactory<ICollection<Trade>> factory =
                new PositionTradesFactory(context, strategy);

            IEnumerable<Trade> trades = factory.Make();

            return trades.Sum(t => t.Amount * t.Price);
        }
    }
}
