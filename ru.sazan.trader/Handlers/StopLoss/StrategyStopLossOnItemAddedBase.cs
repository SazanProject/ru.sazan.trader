﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Events;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers.StopLoss
{
    public abstract class StrategyStopLossOnItemAddedBase<T>:AddedItemHandler<T>, Identified
    {
        protected Strategy strategy;
        protected DataContext tradingData;
        protected ObservableQueue<Signal> signalQueue;
        protected Logger logger;

        protected bool measureFromSignalPrice;
        protected StopPointsSettings spSettings;
        protected StopLossOrderSettings sloSettings;
        protected Signal signal;

        public StrategyStopLossOnItemAddedBase(Strategy strategy,
            DataContext tradingData,
            ObservableQueue<Signal> signalQueue,
            Logger logger,
            bool measureFromSignalPrice = false)
            :base(tradingData.Get<ItemHasBeenAddedNotifier<T>>())
        {
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;
            this.measureFromSignalPrice = measureFromSignalPrice;

            SetStopPointsSettings();
            SetStopLossOrderSettings();
        }

        public override void OnItemAdded(T item)
        {
            this.signal = null;

            if (this.spSettings == null)
                return;

            if (this.sloSettings == null)
                return;

            if (this.tradingData.UnfilledExists(this.strategy, OrderType.Stop) ||
                this.tradingData.UnfilledExists(this.strategy, OrderType.Market))
                return;

            if (this.tradingData.UndeliveredExists(this.strategy, OrderType.Stop) ||
                this.tradingData.UndeliveredExists(this.strategy, OrderType.Market))
                return;

            double amount = this.tradingData.GetAmount(this.strategy);

            if (amount == 0)
                return;

            MakeSignal(item, amount);

            if (this.signal == null)
                return;

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сгенерирован сигнал {2}.",
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name,
                signal.ToString()));

            this.signalQueue.Enqueue(signal);

            this.signal = null;
        }

        public abstract void MakeSignal(T item, double positionAmount);

        private void SetStopLossOrderSettings()
        {
            try
            {
                this.spSettings =
                    this.tradingData.
                    Get<IEnumerable<StopPointsSettings>>().
                    SingleOrDefault(i => i.Strategy.Id == this.strategy.Id);
            }
            catch
            {
                this.spSettings = null;
            }
        }

        private void SetStopPointsSettings()
        {
            try
            {
                this.sloSettings =
                    this.tradingData.
                    Get<IEnumerable<StopLossOrderSettings>>().
                    SingleOrDefault(i => i.Strategy.Id == this.strategy.Id);
            }
            catch
            {
                this.sloSettings = null;
            }
        }

        public int Id
        {
            get { return this.strategy.Id; }
        }
    }
}
