﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Handlers
{
    public class RejectOrderOnOrderRejection:AddedItemHandler<OrderRejection>
    {
        private DataContext tradingData;
        private Logger logger;

        public RejectOrderOnOrderRejection(DataContext tradingData, Logger logger)
            : base(tradingData.Get<ObservableHashSet<OrderRejection>>())
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void OnItemAdded(OrderRejection item)
        {
            if (!OrderExists(item.Order))
                return;

            Order order = this.tradingData.Get<IEnumerable<Order>>().Single(o => o.Id == item.OrderId);
            
            order.Reject(item.DateTime, item.Description);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, заявка отклонена {2}.", DateTime.Now, this.GetType().Name, order.ToString()));
        }

        private bool OrderExists(Order order)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Order>>().Contains(order);
            }
            catch
            {
                return false;
            }
        }

    }
}
