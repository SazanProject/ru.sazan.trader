﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.Handlers
{
    public abstract class AddedItemHandler<T>
    {
        protected ItemHasBeenAddedNotifier<T> notifier;

        public AddedItemHandler(ItemHasBeenAddedNotifier<T> notifier)
        {
            this.notifier = notifier;
            this.notifier.OnItemAdded += new ItemHasBeenAddedNotification<T>(OnItemAdded);
        }

        public abstract void OnItemAdded(T item);
    }
}
