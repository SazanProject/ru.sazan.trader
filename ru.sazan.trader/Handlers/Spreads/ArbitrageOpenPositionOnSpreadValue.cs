﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Handlers.Spreads
{
    public class ArbitrageOpenPositionOnSpreadValue : AddedItemHandler<SpreadValue>
    {
        private ArbitrageSettings arbitrageSetings;
        private Strategy strategy;
        private DataContext tradingData;
        private ObservableQueue<Signal> signalQueue;
        private Logger logger;
        private bool isLeftLegStrategy;

        public ArbitrageOpenPositionOnSpreadValue(ArbitrageSettings arbitrageSettings, Strategy strategy, DataContext tradingData, ObservableQueue<Signal> signalQueue, Logger logger)
            : base(tradingData.Get<ObservableCollection<SpreadValue>>())
        {
            this.arbitrageSetings = arbitrageSettings;
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;

            this.isLeftLegStrategy = arbitrageSettings.LeftLeg.Any(s => s.Id == strategy.Id);
        }

        public override void OnItemAdded(SpreadValue item)
        {
            if (strategy.Amount <= Math.Abs(tradingData.GetAmount(strategy)))
                return;

            if (tradingData.UnfilledExists(strategy, OrderType.Market))
                return;

            TradeAction? action = GetTradeAction(item);

            if (action == null)
                return;

            Signal signal = MakeSignal(item, action.Value);

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сгенерирован {2}.", DateTime.Now, this.GetType().Name, signal.ToString()));

            signalQueue.Enqueue(signal);
        }

        private Signal MakeSignal(SpreadValue item, TradeAction action)
        {
            StrategyVolumeChangeStep strategyVolumeChangeStep = GetStrategyVolumeChangeStep(this.strategy);

            double price = GetPrice(item, action);

            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), action, OrderType.Market, price, 0, 0);

            if (strategyVolumeChangeStep != null)
                signal.Amount = GetAmount(strategyVolumeChangeStep.Amount);

            return signal;
        }

        private StrategyVolumeChangeStep GetStrategyVolumeChangeStep(Strategy strategy)
        {
            return this.tradingData.Get<IEnumerable<StrategyVolumeChangeStep>>().FirstOrDefault(s => s.StrategyId == strategy.Id);
        }

        private TradeAction? GetTradeAction(SpreadValue item)
        {
            if (arbitrageSetings.SpreadSettings.SellAfterPrice <= item.SellAfterPrice)
                return isLeftLegStrategy ? TradeAction.Sell : TradeAction.Buy;

            if (arbitrageSetings.SpreadSettings.BuyBeforePrice >= item.BuyBeforePrice)
                return isLeftLegStrategy ? TradeAction.Buy : TradeAction.Sell;

            return null;
        }

        private double GetPrice(SpreadValue item, TradeAction action)
        {
            if (isLeftLegStrategy)
                return action == TradeAction.Sell ? item.SellAfterPrice : item.BuyBeforePrice;
            else
                return action == TradeAction.Sell ? item.BuyBeforePrice : item.SellAfterPrice;
        }

        private double GetAmount(double step)
        {
            double unfilledAmount = strategy.Amount - Math.Abs(tradingData.GetAmount(strategy));

            if (unfilledAmount < step)
                return unfilledAmount;
            else
                return step;
        }
    }
}
