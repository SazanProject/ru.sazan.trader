﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Extensions;
using System.Collections.Specialized;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Configuration;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Handlers
{
    public class UpdateBarsOnTick:AddedItemHandler<Tick>
    {
        private DataContext tradingData;
        private BarSettings barSettings;
        private TimeTrackable timeTracker;
        private Logger logger;

        public UpdateBarsOnTick(BarSettings barSettings, TimeTrackable timeTracker, DataContext tradingData, Logger logger)
            : base(tradingData.Get<ObservableCollection<Tick>>())
        {
            this.tradingData = tradingData;
            this.barSettings = barSettings;
            this.timeTracker = timeTracker;
            this.logger = logger;
        }

        public override void OnItemAdded(Tick item)
        {
            if (barSettings.Symbol != item.Symbol)
                return;

            DateTime end = GetPeriodEndDate();

            if (this.tradingData.Get<IEnumerable<Tick>>().LastOrDefault(t => t.Symbol == item.Symbol).DateTime < end)
                return;

            if (BarExists(end))
                return;

            if (end > this.timeTracker.StartAt + this.timeTracker.Duration)
                return;

            DateTime begin = end.AddSeconds(-this.barSettings.Interval);

            IEnumerable<Tick> barTicks = GetTicksInRangeOf(item, begin, end);

            if (barTicks == null)
                return;

            if (barTicks.Count() == 0)
                return;

            Bar fresh = BarsFactory.MakeBar(barTicks, end);
            fresh.Interval = this.barSettings.Interval;

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, добавлен новый бар {2}", DateTime.Now, this.GetType().Name, fresh.ToString()));

            this.tradingData.Get<ObservableCollection<Bar>>().Add(fresh);
        }

        private bool BarExists(DateTime date)
        {
            return this.tradingData.Get<IEnumerable<Bar>>().Any(b => b.Symbol == this.barSettings.Symbol
                && b.DateTime.Year == date.Year
                && b.DateTime.Month == date.Month
                && b.DateTime.Day == date.Day
                && b.DateTime.Hour == date.Hour
                && b.DateTime.Minute == date.Minute
                && b.DateTime.Second == date.Second
                && b.DateTime.Millisecond == date.Millisecond);
        }

        private DateTime GetPeriodEndDate()
        {
            DateTime p = this.timeTracker.StartAt.AddSeconds(this.barSettings.Interval);

            DateTime current = this.timeTracker.StartAt + this.timeTracker.Duration;

            if (current < p)
                return p.RoundDownToNearestMinutes(this.barSettings.Interval / 60);

            return current.RoundDownToNearestMinutes(this.barSettings.Interval / 60);
        }

        private IEnumerable<Tick> GetTicksInRangeOf(Tick item, DateTime begin, DateTime end)
        {
            try
            {
                return this.tradingData.Get<IEnumerable<Tick>>().Where(t => t.Symbol == item.Symbol && t.DateTime >= begin && t.DateTime < end);
            }
            catch
            {
                return null;
            }
        }

    }
}
