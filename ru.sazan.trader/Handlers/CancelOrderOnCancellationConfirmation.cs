﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Handlers
{
    public class CancelOrderOnCancellationConfirmation:AddedItemHandler<OrderCancellationConfirmation>
    {
        private DataContext tradingData;
        private Logger logger;

        public CancelOrderOnCancellationConfirmation(DataContext tradingData, Logger logger)
            : base(tradingData.Get<ObservableHashSet<OrderCancellationConfirmation>>()) 
        {
            this.tradingData = tradingData;
            this.logger = logger;
        }

        public override void OnItemAdded(OrderCancellationConfirmation item)
        {
            Order order = this.tradingData.Get<IEnumerable<Order>>().SingleOrDefault(o => o.Id == item.OrderId);

            if (order == null)
                return;

            order.Cancel(item.DateTime, item.Description);
            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, заявка отменена {2}.", DateTime.Now, this.GetType().Name, order.ToString()));
        }
    }
}
