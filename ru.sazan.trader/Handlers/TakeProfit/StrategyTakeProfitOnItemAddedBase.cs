﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Events;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers.TakeProfit
{
    public abstract class StrategyTakeProfitOnItemAddedBase<T>:AddedItemHandler<T>, Identified
    {
        protected Strategy strategy;
        protected DataContext tradingData;
        protected ObservableQueue<Signal> signalQueue;
        protected Logger logger;

        protected ProfitPointsSettings ppSettings;
        protected TakeProfitOrderSettings tpoSettings;
        protected bool measureFromSignalPrice;
        protected Signal signal;

        public StrategyTakeProfitOnItemAddedBase(Strategy strategy,
            DataContext tradingData,
            ObservableQueue<Signal> signalQueue,
            Logger logger,
            bool measureFromSignalPrice = false)
            :base(tradingData.Get<ItemHasBeenAddedNotifier<T>>())
        {
            this.strategy = strategy;
            this.tradingData = tradingData;
            this.signalQueue = signalQueue;
            this.logger = logger;
            this.measureFromSignalPrice = measureFromSignalPrice;

            SetProfitPointsSettings();
            SetTakeProfitOrderSettings();
        }

        public override void OnItemAdded(T item)
        {
            this.signal = null;

            if (this.ppSettings == null)
                return;

            if (this.tpoSettings == null)
                return;

            if (this.tradingData.UnfilledExists(this.strategy, OrderType.Market) ||
                this.tradingData.UnfilledExists(this.strategy, OrderType.Limit))
                return;

            double amount = this.tradingData.GetAmount(this.strategy);

            if (amount == 0)
                return;

            MakeSignal(item, amount);

            if (this.signal == null)
                return;

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, сгенерирован сигнал {2}.",
                BrokerDateTime.Make(DateTime.Now),
                this.GetType().Name,
                signal.ToString()));

            this.signalQueue.Enqueue(signal);

            this.signal = null;
        }

        public abstract void MakeSignal(T item, double positionAmount);

        private void SetTakeProfitOrderSettings()
        {
            this.tpoSettings =
                this.tradingData.
                Get<IEnumerable<TakeProfitOrderSettings>>().
                SingleOrDefault(i => i.Strategy.Id == this.strategy.Id);
        }

        private void SetProfitPointsSettings()
        {
            this.ppSettings = this.tradingData.
                Get<IEnumerable<ProfitPointsSettings>>().
                SingleOrDefault(i => i.Strategy.Id == this.strategy.Id);
        }

        public int Id
        {
            get { return this.strategy.Id; }
        }
    }
}
