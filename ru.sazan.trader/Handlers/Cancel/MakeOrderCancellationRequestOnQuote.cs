﻿using ru.sazan.trader.Collections;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.Handlers.Cancel
{
    public class MakeOrderCancellationRequestOnQuote : QuotesHandler
    {
        private Strategy strategy;
        private double priceShift;
        private QuoteProvider quotesProvider;
        private DataContext tradingData;
        private Logger logger;

        private double bestPrice;
        private OrderCancellationRequest request;

        public MakeOrderCancellationRequestOnQuote(Strategy strategy,
            double priceShiftPoints)
            : this(strategy, priceShiftPoints, OrderBook.Instance, TradingData.Instance, new NullLogger()) { }

        public MakeOrderCancellationRequestOnQuote(Strategy strategy,
            double priceShiftPoints,
            QuoteProvider quotesProvider,
            DataContext tradingData,
            Logger logger)
            : base(quotesProvider)
        {
            this.strategy = strategy;
            this.priceShift = priceShiftPoints;
            this.quotesProvider = quotesProvider;
            this.tradingData = tradingData;
            this.logger = logger;

            this.bestPrice = 0;
            this.request = null;
        }

        public override void OnQuotesUpdate(string symbol)
        {
            if (!this.strategy.Symbol.Equals(symbol))
                return;

            if (this.tradingData.GetAmount(this.strategy) != 0)
                return;

            Order lastUnfilledOrder = GetLastUnfilledOrder();

            if (lastUnfilledOrder == null)
                return;

            if (!lastUnfilledOrder.IsDelivered)
                return;

            if (!BestQuotePriceGoneOverTheLimit(lastUnfilledOrder))
                return;

            if (OrderCancellationRequstExists(lastUnfilledOrder))
                return;

            this.request = null;
            this.request = MakeOrderCancellationRequest(lastUnfilledOrder);             

            this.logger.Log(String.Format("{0:dd/MM/yyyy H:mm:ss.fff}, {1}, запрос на снятие заявки {2}", DateTime.Now, this.GetType().Name, request.ToString()));

            this.tradingData.Get<ObservableHashSet<OrderCancellationRequest>>().Add(request);

        }

        private Order GetLastUnfilledOrder()
        {
            try
            {
                return this.tradingData.GetUnfilled(this.strategy, OrderType.Limit).Last();
            }
            catch
            {
                return null;
            }
        }

        private bool BestQuotePriceGoneOverTheLimit(Order order)
        {
            if (order.TradeAction == TradeAction.Buy)
            {
                this.bestPrice = quotesProvider.GetBidPrice(order.Symbol, 0);

                return order.Price + priceShift <= bestPrice;
            }
            else
            {
                this.bestPrice = quotesProvider.GetOfferPrice(order.Symbol, 0);

                return order.Price - priceShift >= bestPrice;
            }
        }

        private bool OrderCancellationRequstExists(Order order)
        {
            return this.tradingData.Get<IEnumerable<OrderCancellationRequest>>().Any(o => o.OrderId == order.Id
                && o.DateTime.AddSeconds(60) > BrokerDateTime.Make(DateTime.Now));
        }

        private OrderCancellationRequest MakeOrderCancellationRequest(Order order)
        {
            return new OrderCancellationRequest(order,
                String.Format("Лучшая цена ушла от лимита заявки на {0} пунктов!", this.priceShift));
        }

    }


}
