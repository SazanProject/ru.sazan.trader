﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.Data
{
    public class PositionTradesFactory:GenericFactory<ICollection<Trade>>
    {
        private DataContext tradingData;
        private Strategy strategy;
        private double positionAmount;
        private PositionType positionType;
        private Trade[] strategyTrades;

        public PositionTradesFactory(DataContext tradingData, Strategy strategy)
        {
            this.tradingData = tradingData;
            this.strategy = strategy;

            this.positionAmount = this.tradingData.GetAmount(this.strategy);

            this.positionType = GetPositionType(this.positionAmount);

            this.strategyTrades = this.tradingData.GetTrades(this.strategy).ToArray();
        }

        private PositionType GetPositionType(double amount)
        {
            if (amount > 0)
                return PositionType.Long;

            if (amount < 0)
                return PositionType.Short;

            return PositionType.Any;
        }

        public ICollection<Trade> Make()
        {
            List<Trade> result = new List<Trade>();

            double resultAmount = 0;

            if (this.positionAmount == 0)
                return result;

            for (int i = this.strategyTrades.Length - 1; i >= 0; i--)
            {
                if (resultAmount == this.positionAmount)
                    break;

                if (this.positionType == PositionType.Long && this.strategyTrades[i].Sell)
                    continue;

                if (this.positionType == PositionType.Short && this.strategyTrades[i].Buy)
                    continue;
                
                Trade clone = MakeTradeClone(this.strategyTrades[i], this.positionAmount - resultAmount);
                result.Add(clone);
                resultAmount += clone.Amount;
            }

            return result;
        }

        private Trade MakeTradeClone(Trade src, double restAmount)
        {
            Trade clone = new Trade(src);

            if (Math.Abs(src.Amount) > Math.Abs(restAmount))
                clone.Amount = restAmount;

            return clone;
        }
    }
}
