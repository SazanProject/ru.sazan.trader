﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Data
{
    public class SymbolDataContext:RawBaseDataContext
    {
        public HashSetOfNamedMutable<SymbolSettings> SymbolSettings { get; private set; }
        public HashSetOfNamedMutable<SymbolSummary> SymbolSummary { get; private set; }

        public SymbolDataContext()
        {
            this.SymbolSettings = new HashSetOfNamedMutable<SymbolSettings>();
            this.SymbolSummary = new HashSetOfNamedMutable<SymbolSummary>();
        }
    }
}
