﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public interface StrictDataContext
    {
        HashSet<T> GetData<T>() where T : class;
    }
}
