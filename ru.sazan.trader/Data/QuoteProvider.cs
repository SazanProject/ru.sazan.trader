﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public interface QuoteProvider:QuoteUpdateManager, SymbolDataHasBeenUpdatedNotifier
    {
        double GetBidPrice(string name, int position);
        double GetOfferPrice(string name, int position);
        double GetBidVolume(string name, int position);
        double GetOfferVolume(string name, int position);
    }
}
