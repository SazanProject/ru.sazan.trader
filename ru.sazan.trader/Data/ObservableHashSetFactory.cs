﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Data
{
    public interface ObservableHashSetFactory
    {
        ObservableHashSet<T> Make<T>();
    }
}
