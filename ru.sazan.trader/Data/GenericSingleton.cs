﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Data
{
    public interface GenericSingleton<T>
        where T : class
    {
        T Instance { get; }
        void Destroy();
        bool IsNull { get; }
    }
}
