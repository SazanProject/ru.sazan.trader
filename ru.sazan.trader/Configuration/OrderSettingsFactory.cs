﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class OrderSettingsFactory:GenericFactory<OrderSettings>
    {
        private Strategy strategy;
        private string prefix;

        public OrderSettingsFactory(Strategy strategy, string prefix)
        {
            this.strategy = strategy;
            this.prefix = prefix;
        }

        public OrderSettings Make()
        {
            try
            {
                return new OrderSettings(this.strategy,
                    AppSettings.GetValue<int>(String.Concat(this.prefix, "_OrderSettings_TimeToLive")));
            }
            catch
            {
                return null;
            }
        }
    }
}
