﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Configuration
{
    public class StrategyFactory:GenericFactory<Strategy>
    {
        private string prefix;

        private StrategyFactory() { }

        public StrategyFactory(string prefix)
        {
            this.prefix = prefix;
        }

        public Strategy Make()
        {
            try
            {
                return new Strategy(AppSettings.GetValue<int>(String.Concat(this.prefix, "_Strategy_Id")),
                    AppSettings.GetStringValue(String.Concat(this.prefix, "_Strategy_Description")),
                    AppSettings.GetStringValue(String.Concat(this.prefix, "_Strategy_Portfolio")),
                    AppSettings.GetStringValue(String.Concat(this.prefix, "_Strategy_Symbol")),
                    AppSettings.GetValue<double>(String.Concat(this.prefix, "_Strategy_Amount")));
            }
            catch
            {
                return null;
            }
        }
    }
}
