﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class TakeProfitOrderSettings:OrderSettings
    {
        private TakeProfitOrderSettings() { }

        public TakeProfitOrderSettings(Strategy strategy, int timeToLiveSeconds) :
            base(strategy, timeToLiveSeconds) { }
    }
}
