﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Utility;
using System.Globalization;

namespace ru.sazan.trader.Models
{
    public class Correlation:TimeStamped
    {
        public DateTime DateTime { get; set; }
        public int StrategyId { get; set; }
        public Strategy Strategy { get; set; }
        public double Value { get; set; }

        public Correlation(Strategy strategy, DateTime date, double value)
        {
            this.Strategy = strategy;
            this.StrategyId = this.Strategy.Id;
            this.DateTime = date;
            this.Value = value;
        }

        public override string ToString()
        {
            CultureInfo ci = CultureInfo.InvariantCulture;

            return String.Format("Correlation: {0}, {1}, {2}", this.StrategyId,
                this.DateTime.ToString(ci),
                this.Value.ToString("0.0000", ci));

        }
    }
}
