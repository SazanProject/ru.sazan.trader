﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace ru.sazan.trader.Models
{
    public class Bar
    {
        public string Symbol { get; set; }

        public int Interval { get; set; }

        public DateTime DateTime { get; set; }

        public double Open { get; set; }

        public double High { get; set; }

        public double Low { get; set; }

        public double Close { get; set; }

        public double Volume { get; set; }

        public Bar() { }

        public Bar(DateTime dateTime, double open, double high, double low, double close, double volume)
        :this("", 0, dateTime, open, high, low, close, volume){}

        public Bar(string symbol, int interval, DateTime dateTime, double open, double high, double low, double close, double volume)
        {
            this.Symbol = symbol;
            this.Interval = interval;
            this.DateTime = dateTime;
            this.Open = open;
            this.High = high;
            this.Low = low;
            this.Close = close;
            this.Volume = volume;
        }

        public static Bar Parse(string barString)
        {
            string[] parts = barString.Split(',');

            CultureInfo provider = CultureInfo.InvariantCulture;

            string pattern = "yyyyMMdd HHmmss";

            string dateTime = string.Empty;

            if (parts.Count() == 9)
            {
                dateTime = String.Concat(parts[2], " ", parts[3]);

                return new Bar
                {
                    Symbol = parts[0],
                    Interval = Convert.ToInt16(parts[1], provider) * 60,
                    DateTime = DateTime.ParseExact(dateTime, pattern, provider),
                    Open = Convert.ToDouble(parts[4], provider),
                    High = Convert.ToDouble(parts[5], provider),
                    Low = Convert.ToDouble(parts[6], provider),
                    Close = Convert.ToDouble(parts[7], provider),
                    Volume = Convert.ToDouble(parts[8], provider)
                };
            }
            else if (parts.Count() == 8)
            {
                dateTime = String.Concat(parts[1], " ", parts[2]);

                return new Bar
                {
                    Symbol = parts[0],
                    DateTime = DateTime.ParseExact(dateTime, pattern, provider),
                    Open = Convert.ToDouble(parts[3], provider),
                    High = Convert.ToDouble(parts[4], provider),
                    Low = Convert.ToDouble(parts[5], provider),
                    Close = Convert.ToDouble(parts[6], provider),
                    Volume = Convert.ToDouble(parts[7], provider)
                };
            }

            dateTime = String.Concat(parts[0], " ", parts[1]);

            return new Bar(DateTime.ParseExact(dateTime, pattern, provider),
                Convert.ToDouble(parts[2], provider),
                Convert.ToDouble(parts[3], provider),
                Convert.ToDouble(parts[4], provider),
                Convert.ToDouble(parts[5], provider),
                Convert.ToDouble(parts[6], provider));
        }

        private static bool FirstFieldContainsDate(string barString)
        {
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;

                string pattern = "yyyyMMdd";

                string first = barString.Split(',').ElementAt(0);

                DateTime date = DateTime.ParseExact(first, pattern, provider);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public override string ToString()
        {
            return ToString("Symbol: {0}, Interval: {1}, DateTime: {2}, Open: {3}, High: {4}, Low: {5}, Close: {6}, Volume: {7}");
        }

        public string ToImportString()
        {
            return ToString("{0},{1},{2:yyyyMMdd,HHmmss},{3},{4},{5},{6},{7}");
        }

        private string ToString(string format)
        {
            CultureInfo ci = CultureInfo.InvariantCulture;

            return String.Format(format, this.Symbol, this.Interval, this.DateTime.ToString(ci), this.Open.ToString("0.0000", ci), this.High.ToString("0.0000", ci), this.Low.ToString("0.0000", ci), this.Close.ToString("0.0000", ci), this.Volume.ToString("0.0000", ci));
        }

        public bool IsWhite
        {
            get
            {
                return this.Close > this.Open;
            }
        }

        public bool IsBlack
        {
            get
            {
                return this.Close < this.Open;
            }
        }
    }
}
