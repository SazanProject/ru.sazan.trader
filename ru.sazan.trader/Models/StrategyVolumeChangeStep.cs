﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class StrategyVolumeChangeStep: Identified
    {
        public Strategy Strategy { get; set; }

        public double Amount { get; set; }

        public int StrategyId { get; set; }

        public int Id { get; set; }

        public StrategyVolumeChangeStep(Strategy strategy, double amount)
        {
            this.Strategy = strategy;
            this.StrategyId = strategy.Id;
            this.Amount = amount;
            this.Id = strategy.Id;
        }
    }
}
