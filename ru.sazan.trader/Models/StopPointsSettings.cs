﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Models
{
    public class StopPointsSettings:PointsSettings
    {
        private StopPointsSettings() { }

        public StopPointsSettings(Strategy strategy, double points, bool trail)
            : base(strategy, points, trail) { }
    }
}
