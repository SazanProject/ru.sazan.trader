﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class StopLossOrderSettings:OrderSettings
    {
        private StopLossOrderSettings() { }

        public StopLossOrderSettings(Strategy strategy, int timeToLiveSeconds) :
            base(strategy, timeToLiveSeconds) { }
    }
}
