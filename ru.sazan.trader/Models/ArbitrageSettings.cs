﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Models
{
    public class ArbitrageSettings:Identified
    {
        public ICollection<Strategy> LeftLeg { get; set; }
        public ICollection<Strategy> RightLeg { get; set; }

        public SpreadSettings SpreadSettings { get; set; }

        public int Id { get; set; }

        public ArbitrageSettings()
            :this(SerialIntegerFactory.Make(),
            new List<Strategy>(),
            new List<Strategy>(),
            new SpreadSettings())
        {
        }

        public ArbitrageSettings(int id, ICollection<Strategy> leftLeg, ICollection<Strategy> rightLeg, SpreadSettings spreadSettings)
        {
            Id = id;
            LeftLeg = leftLeg;
            RightLeg = rightLeg;
            SpreadSettings = spreadSettings;
        }

        public bool HasSymbol(string symbol)
        {
            return LeftLeg.Any(s => s.Symbol.Equals(symbol)) || RightLeg.Any(s => s.Symbol.Equals(symbol));
        }
    }
}
