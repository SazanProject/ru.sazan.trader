﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public class NamedEqualityComparer<T> : IEqualityComparer<T>
        where T : Named
    {
        public bool Equals(T left, T right)
        {
            if (left.Name == right.Name)
                return true;

            return false;
        }

        public int GetHashCode(T item)
        {
            return item.Name.GetHashCode();
        }
    }
}
