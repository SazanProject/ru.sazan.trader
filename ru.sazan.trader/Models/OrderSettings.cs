﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;

namespace ru.sazan.trader.Models
{
    public class OrderSettings:Identified
    {
        public int Id { get; set; }
        public int TimeToLive { get; set; }
        public Strategy Strategy { get; set; }
        public int StrategyId { get; set; }

        protected OrderSettings() { }

        public OrderSettings(Strategy strategy, int timeToLiveSeconds)
        {
            this.Id = strategy.Id;
            this.TimeToLive = timeToLiveSeconds;
            this.Strategy = strategy;
            this.StrategyId = strategy.Id;
        }
    }
}
