﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Models
{
    public interface HistoryDataRequest
    {
        string Symbol { get; }
        int Interval { get; }
        int Quantity { get; }
        DateTime FromDate { get; }
    }
}
