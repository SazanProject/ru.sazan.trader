﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using System.IO;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.Utility
{
    public class ImportTicksTransaction : ImportItemsTransaction<Tick>
    {
        private string symbol;
        public ImportTicksTransaction(ObservableCollection<Tick> dst, string path, string symbol = "")
            :base(dst, path)
        {
            this.symbol = symbol;
        }

        public override Tick TryParseItem(string src)
        {
            try
            {
                Tick item = Tick.Parse(src);

                if (!string.IsNullOrEmpty(this.symbol))
                    item.Symbol = this.symbol;

                return item;
            }
            catch
            {
                return null;
            }
        }
    }

}
