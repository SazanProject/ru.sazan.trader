﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ru.sazan.trader.Utility
{
    public interface TimeTrackable
    {
        DateTime StartAt { get; }
        TimeSpan Duration { get; }
    }
}
