﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.tests.Mocks
{
    public delegate void MockEvent();

    public class MockEventBinder : GenericBinder
    {
        public int BindedEventsCounter { get; set; }

        private MockEvent mockEvent;

        public void Bind<T>(T method)
        {
            if (method is MockEvent)
            {
                this.mockEvent += method as MockEvent;
                this.BindedEventsCounter++;
            }
        }

        public void Unbind<T>(T method)
        {
            if (this.BindedEventsCounter == 0)
                return;

            if (method is MockEvent)
            {
                this.mockEvent -= method as MockEvent;
                this.BindedEventsCounter--;
            }
        }
    }
}
