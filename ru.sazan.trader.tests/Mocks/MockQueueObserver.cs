﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Events;

namespace ru.sazan.trader.tests.Mocks
{
    public class MockQueueObserver : Observer
    {

        private string data = "No data";
        public string Data
        {
            get
            {
                return this.data;
            }
        }

        private ObservableQueue<string> queue;

        public MockQueueObserver(ObservableQueue<string> queue)
        {
            this.queue = queue;
        }

        public void Update()
        {
            this.data = this.queue.Dequeue();
        }
    }
}
