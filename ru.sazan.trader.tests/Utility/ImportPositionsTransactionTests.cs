﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class ImportPositionsTransactionTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
        }

        [TestMethod]
        public void import_positions()
        {
            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Position>>().Count());

            string ef = String.Concat(ProjectRootFolderNameFactory.Make(), "\\positions.txt");
            Transaction import = new ImportPositionsTransaction((ObservableHashSetFactory)this.tradingData, ef);

            import.Execute();

            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Position>>().Count());
        }
    }
}
