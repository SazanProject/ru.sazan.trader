﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Collections;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class TickCollectionFactoryTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
        }

        [TestMethod]
        public void Tick_Import()
        {
            GenericFactory<IEnumerable<Tick>> factory = new TickCollectionFactory(ProjectRootFolderNameFactory.Make() + "\\SPFB.SBRF_130802_130802.txt");
            
            IEnumerable<Tick> collection = factory.Make();

            Assert.AreEqual(57256, collection.Count());
            Assert.AreEqual(9826, collection.ElementAt(0).Price);
            Assert.AreEqual(9837, collection.ElementAt(28).Price);
            Assert.AreEqual(9798, collection.ElementAt(57254).Price);
            Assert.AreEqual(9795, collection.ElementAt(57255).Price);
        }

        [TestMethod]
        public void Tick_Import_with_assign_symbol_name_test()
        {
            string symbol = "SBRF-3.14_FT";

            Transaction tickImportTransaction = new ImportTicksTransaction(this.tradingData.Get<ObservableCollection<Tick>>(), 
                ProjectRootFolderNameFactory.Make() + "\\SPFB.SBRF_130802_130802.txt",
                symbol);

            tickImportTransaction.Execute();

            Assert.AreEqual(57256, this.tradingData.Get<IEnumerable<Tick>>().Count());
            Assert.AreEqual(symbol, this.tradingData.Get<IEnumerable<Tick>>().First().Symbol);
            Assert.AreEqual(9826, this.tradingData.Get<IEnumerable<Tick>>().First().Price);
            Assert.AreEqual(9795, this.tradingData.Get<IEnumerable<Tick>>().Last().Price);
        }
    }
}
