﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Models;
using ru.sazan.trader.Utility;

namespace ru.sazan.trader.tests.Utility
{
    [TestClass]
    public class ImportTradesTransactionTests
    {
        private DataContext tradingData;
        private string path;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
            this.path = String.Concat(ProjectRootFolderNameFactory.Make(), "\\trades.txt");
        }

        [TestMethod]
        public void ImportTradesTransaction_import_three_orders()
        {
            Strategy strategy = new Strategy(1, "First strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Strategy strategy1 = new Strategy(2, "Second strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy1);

            Strategy strategy2 = new Strategy(3, "Third strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy2);

            Signal s1 = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, 150000, 0, 150000);
            s1.Id = 1;
            this.tradingData.Get<ICollection<Signal>>().Add(s1);

            Signal s2 = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, 150000, 0, 0);
            s2.Id = 2;
            this.tradingData.Get<ICollection<Signal>>().Add(s2);

            Signal s3 = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Stop, 150000, 150000, 0);
            s3.Id = 3;
            this.tradingData.Get<ICollection<Signal>>().Add(s3);

            Order o1 = new Order(s1);
            o1.Id = 1;
            this.tradingData.Get<ICollection<Order>>().Add(o1);

            Order o2 = new Order(s2);
            o2.Id = 2;
            this.tradingData.Get<ICollection<Order>>().Add(o2);

            Order o3 = new Order(s3);
            o3.Id = 3;
            this.tradingData.Get<ICollection<Order>>().Add(o3);

            Transaction import = new ImportTradesTransaction((ObservableHashSetFactory)this.tradingData, this.path);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Trade>>().Count());

            import.Execute();

            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Trade>>().Count());
        }

        [TestMethod]
        public void import_trades_only_for_existent_orderss()
        {
            Strategy strategy = new Strategy(1, "First strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(strategy);

            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            signal.Id = 1;
            this.tradingData.Get<ICollection<Signal>>().Add(signal);

            Order o1 = new Order(signal);
            o1.Id = 1;
            this.tradingData.Get<ICollection<Order>>().Add(o1);

            Assert.AreEqual(0, this.tradingData.Get<IEnumerable<Trade>>().Count());

            Transaction import = new ImportTradesTransaction((ObservableHashSetFactory)this.tradingData, this.path);

            import.Execute();

            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<Trade>>().Count());
        }

    }
}
