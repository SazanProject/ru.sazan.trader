﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Data;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;
using ru.sazan.trader.tests.Mocks;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;
using ru.sazan.trader.Handlers.StopLoss;
using ru.sazan.trader.Handlers.TakeProfit;

namespace ru.sazan.trader.tests.TraderBaseTests
{
    [TestClass]
    public class OpenLongByMarketCloseByLimitTest:TraderBaseInitializer
    {

        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            Symbol symbol = new Symbol("RTS-9.13_FT", 1, 8, 10, BrokerDateTime.Make(DateTime.Now));
            this.tradingData.Get<HashSetOfNamedMutable<Symbol>>().Add(symbol);

            this.strategy = new Strategy(10, "strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);
            this.tradingData.Get<ICollection<Strategy>>().Add(this.strategy);

            StopPointsSettings slSettings = new StopPointsSettings(this.strategy, 300, false);
            this.tradingData.Get<ICollection<StopPointsSettings>>().Add(slSettings);

            ProfitPointsSettings tpSettings = new ProfitPointsSettings(this.strategy, 500, false);
            this.tradingData.Get<ICollection<ProfitPointsSettings>>().Add(tpSettings);

            StopLossOrderSettings slOrderSettings = new StopLossOrderSettings(this.strategy, 3600);
            this.tradingData.Get<ICollection<StopLossOrderSettings>>().Add(slOrderSettings);

            TakeProfitOrderSettings tpOrderSettings = new TakeProfitOrderSettings(this.strategy, 3600);
            this.tradingData.Get<ICollection<TakeProfitOrderSettings>>().Add(tpOrderSettings);

            StrategyStopLossByPointsOnTick stopLossHandler =
                new StrategyStopLossByPointsOnTick(strategy, this.tradingData, this.signalQueue, new NullLogger());
            StrategyTakeProfitByPointsOnTick takeProfitHandler =
                new StrategyTakeProfitByPointsOnTick(strategy, this.tradingData, this.signalQueue, new NullLogger());

            PlaceStrategyStopLossByPointsOnTrade placeStopOnTradeHandler =
                new PlaceStrategyStopLossByPointsOnTrade(strategy, this.tradingData, this.signalQueue, new NullLogger());
            PlaceStrategyTakeProfitByPointsOnTrade placeTakeProfitOnTradeHandler =
                new PlaceStrategyTakeProfitByPointsOnTrade(strategy, this.tradingData, this.signalQueue, new NullLogger());

        }

        [TestMethod]
        public void open_long_position_with_market_order_protect_it_with_stop_and_limit_and_close_with_limit()
        {
            // Сигнал на открытие позиции
            Signal inputSignal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            EmulateTradeFor(inputSignal, 150050);

            // Заявка исполнена, позиция открыта ровно на запрошенный в заявке объем
            Assert.AreEqual(10, this.tradingData.GetAmount(this.strategy));

            // Для позиции созданы и отправлены брокеру защитные стоп и тейк профит приказы
            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Signal>>().Count());
            Assert.AreEqual(3, this.tradingData.Get<IEnumerable<Order>>().Count());
            Signal slSignal = this.tradingData.Get<IEnumerable<Signal>>().Single(s => s.OrderType == OrderType.Stop && s.StrategyId == this.strategy.Id);
            Signal tpSignal = this.tradingData.Get<IEnumerable<Signal>>().Single(s => s.OrderType == OrderType.Limit && s.StrategyId == this.strategy.Id);

            // Цена защитных приказов установлена соответственно настройкам
            Assert.AreEqual(149750, slSignal.Stop);
            Assert.AreEqual(150550, tpSignal.Limit);

            // Через некоторое время цена на рынке вырастает, срабатывает take profit приказ и исполняется одной сделкой
            EmulateTradeFor(tpSignal, 145500);

            Order slOrder = 
                this.tradingData.Get<IEnumerable<Order>>().Single(o => o.SignalId == slSignal.Id);
            Order tpOrder = 
                this.tradingData.Get<IEnumerable<Order>>().Single(o => o.SignalId == tpSignal.Id);

            Assert.IsTrue(tpOrder.IsFilled);
            Assert.IsFalse(slOrder.IsFilled);
            Assert.IsFalse(slOrder.IsCanceled);

            // Позиция закрыта
            Assert.AreEqual(0, this.tradingData.GetAmount(this.strategy));
            
            // Брокеру отправлен запрос на отмену stop loss приказа
            Assert.AreEqual(1, this.tradingData.Get<IEnumerable<OrderCancellationRequest>>().Count());            
            OrderCancellationRequest slRequest = this.tradingData.Get<IEnumerable<OrderCancellationRequest>>().Single(r => r.OrderId == slOrder.Id);
            Assert.IsNotNull(slRequest);
        }
    }
}
