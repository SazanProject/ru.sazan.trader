﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradingDataContextGetFilledPartiallyTests
    {
        private DataContext tradingData;

        [TestInitialize]
        public void Setup()
        {
            this.tradingData = new TradingDataContext();
        }

        [TestMethod]
        public void TradingDataContext_GetFilledPartially_limit_orders()
        {
            Strategy st1 = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 8);
            this.tradingData.Get<ICollection<Strategy>>().Add(st1);

            Signal s1 = new Signal(st1, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Limit, 150000, 0, 150000);
            this.tradingData.Get<ICollection<Signal>>().Add(s1);

            Order o1 = new Order(s1);
            o1.FilledAmount = 2;
            Assert.IsFalse(o1.IsFilled);
            Assert.IsTrue(o1.IsFilledPartially);
            Assert.IsFalse(o1.IsCanceled);
            Assert.IsFalse(o1.IsExpired);
            Assert.IsFalse(o1.IsRejected);

            this.tradingData.Get<ICollection<Order>>().Add(o1);

            IEnumerable<Order> unfilled = this.tradingData.GetFilledPartially(st1, OrderType.Limit);

            Assert.AreEqual(1, unfilled.Count());

            Assert.AreSame(unfilled.Last(), o1);

        }

        [TestMethod]
        public void TradingDataContext_GetFilledPartially_market_orders()
        {
            Strategy st1 = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 8);
            this.tradingData.Get<ICollection<Strategy>>().Add(st1);

            Signal s1 = new Signal(st1, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(s1);

            Order o1 = new Order(s1);
            o1.FilledAmount = 2;
            Assert.IsFalse(o1.IsFilled);
            Assert.IsTrue(o1.IsFilledPartially);
            Assert.IsFalse(o1.IsCanceled);
            Assert.IsFalse(o1.IsExpired);
            Assert.IsFalse(o1.IsRejected);

            this.tradingData.Get<ICollection<Order>>().Add(o1);

            IEnumerable<Order> unfilled = this.tradingData.GetFilledPartially(st1, OrderType.Market);

            Assert.AreEqual(1, unfilled.Count());

            Assert.AreSame(unfilled.Last(), o1);

        }

        [TestMethod]
        public void TradingDataContext_GetFilledPartially_stop_orders()
        {
            Strategy st1 = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 8);
            this.tradingData.Get<ICollection<Strategy>>().Add(st1);

            Signal s1 = new Signal(st1, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Stop, 150000, 150000, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(s1);

            Order o1 = new Order(s1);
            o1.FilledAmount = 2;
            Assert.IsFalse(o1.IsFilled);
            Assert.IsTrue(o1.IsFilledPartially);
            Assert.IsFalse(o1.IsCanceled);
            Assert.IsFalse(o1.IsExpired);
            Assert.IsFalse(o1.IsRejected);

            this.tradingData.Get<ICollection<Order>>().Add(o1);

            IEnumerable<Order> unfilled = this.tradingData.GetFilledPartially(st1, OrderType.Stop);

            Assert.AreEqual(1, unfilled.Count());

            Assert.AreSame(unfilled.Last(), o1);

        }

        [TestMethod]
        public void TradingDataContext_GetFilledPartially_returns_null()
        {
            Strategy st1 = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 8);
            this.tradingData.Get<ICollection<Strategy>>().Add(st1);

            Signal s1 = new Signal(st1, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Stop, 150000, 150000, 0);
            this.tradingData.Get<ICollection<Signal>>().Add(s1);

            Order o1 = new Order(s1);
            o1.FilledAmount = 2;
            Assert.IsFalse(o1.IsFilled);
            Assert.IsTrue(o1.IsFilledPartially);
            Assert.IsFalse(o1.IsCanceled);
            Assert.IsFalse(o1.IsExpired);
            Assert.IsFalse(o1.IsRejected);

            this.tradingData.Get<ICollection<Order>>().Add(o1);

            IEnumerable<Order> unfilled = this.tradingData.GetFilledPartially(st1, OrderType.Limit);

            Assert.AreEqual(0, unfilled.Count());

        }
    }
}
