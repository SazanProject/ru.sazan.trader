﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradeCollectionExtensionsSubtractTests:TraderBaseInitializer
    {
        private Strategy strategy;
        private List<Trade> trades;

        [TestInitialize]
        public void Setup()
        {
            this.trades = new List<Trade>();
            this.strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 1);
        }

        [TestMethod]
        public void TradeCollectionExtensions_Combine_clean_entire_collection_test()
        {
            Signal signal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 138000, 0, 0);
            EmulateTradeFor(signal);
            this.trades.Add(this.tradingData.Get<IEnumerable<Trade>>().Last());
            Assert.AreEqual(1, this.trades.Count);

            Signal cClose = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 139000, 0, 0);
            EmulateTradeFor(cClose);
            Trade lastTrade = this.tradingData.Get<IEnumerable<Trade>>().Last();

            this.trades.Combine(lastTrade);
            Assert.AreEqual(0, this.trades.Count);
        }

        [TestMethod]
        public void TradeCollectionExtensions_Combine_remove_just_first_trade_from_collection_test()
        {
            Signal signal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 138000, 0, 0);
            EmulateTradeFor(signal);
            
            this.trades.Add(this.tradingData.Get<IEnumerable<Trade>>().Last());
            Assert.AreEqual(1, this.trades.Count);

            Signal s2 = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 138000, 0, 0);
            EmulateTradeFor(s2);

            Trade second = this.tradingData.Get<IEnumerable<Trade>>().Last();
            this.trades.Add(second);
            Assert.AreEqual(2, this.trades.Count);

            Signal cClose = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 139000, 0, 0);
            EmulateTradeFor(cClose);
            Trade lastTrade = this.tradingData.Get<IEnumerable<Trade>>().Last();

            this.trades.Combine(lastTrade);
            Assert.AreEqual(1, this.trades.Count);

            Assert.AreSame(second, this.trades.Last());
        }

        [TestMethod]
        public void TradeCollectionExtensions_Combine_remove_first_three_trades_from_collection_test()
        {
            Strategy strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 4);
            Signal signal = new Signal(strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 10500, 0, 0);
            EmulateTradeFor(signal, 10500, 10);
            EmulateTradeFor(signal, 10500, 5);
            EmulateTradeFor(signal, 10500, 5);

            this.trades.Add(this.tradingData.Get<IEnumerable<Trade>>().Last());
            Assert.AreEqual(1, this.trades.Count);

            Signal s2 = new Signal(strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 10500, 0, 0);
            EmulateTradeFor(s2);

            Trade second = this.tradingData.Get<IEnumerable<Trade>>().Last();
            this.trades.Add(second);
            Assert.AreEqual(2, this.trades.Count);

            Signal cClose = new Signal(strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 10500, 0, 0);
            EmulateTradeFor(cClose);
            Trade lastTrade = this.tradingData.Get<IEnumerable<Trade>>().Last();

            this.trades.Combine(lastTrade);
            Assert.AreEqual(1, this.trades.Count);

            Assert.AreSame(second, this.trades.Last());
        }
    }
}
