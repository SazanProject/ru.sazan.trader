﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Data;
using ru.sazan.trader.Utility;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradingDataContextGetOpenOrdersTests : TraderBaseInitializer
    {
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = this.tradingData.Get<ICollection<Strategy>>().Single(s => s.Id == 1);
            Assert.IsNotNull(this.strategy);
        }

        [TestMethod]
        public void TradingDataContext_GetOpenOrders_returns_open_orders_for_strategy()
        {
            Assert.AreEqual(0, this.tradingData.GetOpenOrders(this.strategy).Count());

            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.signalQueue.Enqueue(signal);

            Assert.AreEqual(1, this.tradingData.GetOpenOrders(this.strategy).Count());
        }

        [TestMethod]
        public void TradingDataContext_GetOpenOrders_doesnt_returns_other_strategy_orders()
        {
            Assert.AreEqual(0, this.tradingData.GetOpenOrders(this.strategy).Count());

            Strategy strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 2);

            Signal signal = new Signal(strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.signalQueue.Enqueue(signal);

            Assert.AreEqual(0, this.tradingData.GetOpenOrders(this.strategy).Count());
        }

        [TestMethod]
        public void TradingDataContext_GetOpenOrders_doesnt_returns_strategy_close_orders()
        {
            Assert.AreEqual(0, this.tradingData.GetOpenOrders(this.strategy).Count());

            Signal signal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            this.signalQueue.Enqueue(signal);
            
            Order order = this.tradingData.Get<IEnumerable<Order>>().Single(o => o.SignalId == signal.Id);
            Assert.IsNotNull(order);

            Trade trade = new Trade(order, order.Portfolio, order.Symbol, 150010, this.strategy.Amount, BrokerDateTime.Make(DateTime.Now));
            this.tradingData.Get<ObservableHashSet<Trade>>().Add(trade);

            Signal closeSignal = new Signal(this.strategy, BrokerDateTime.Make(DateTime.Now), TradeAction.Sell, OrderType.Market, 151000, 0, 0);
            this.signalQueue.Enqueue(closeSignal);
            
            Assert.AreEqual(1, this.tradingData.GetOpenOrders(this.strategy).Count());
            Assert.AreEqual(TradeAction.Buy, this.tradingData.GetOpenOrders(this.strategy).First().TradeAction);
        }

    }
}
