﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradingDataContextIsOpeningTradeTests:TraderBaseInitializer
    {
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 1);
        }

        [TestMethod]
        public void first_trade_is_opening_test()
        {
            Signal signal = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 150000, 0, 0);
            EmulateTradeFor(signal);

            Trade trade = this.tradingData.Get<IEnumerable<Trade>>().Last();

            Assert.IsTrue(this.tradingData.IsOpening(trade));
        }

        [TestMethod]
        public void last_trade_is_not_opening_trade_test()
        {
            Signal signal = new Signal(this.strategy, DateTime.Now, TradeAction.Sell, OrderType.Market, 150000, 0, 0);
            EmulateTradeFor(signal);

            Signal close = new Signal(this.strategy, DateTime.Now, TradeAction.Buy, OrderType.Market, 149000, 0, 0);
            EmulateTradeFor(close);

            Trade trade = this.tradingData.Get<IEnumerable<Trade>>().Last();

            Assert.IsFalse(this.tradingData.IsOpening(trade));
        }
    }
}
