﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using ru.sazan.trader.Extensions;
using ru.sazan.trader.Emulation;

namespace ru.sazan.trader.tests.Extensions
{
    [TestClass]
    public class TradingDataContextExtensionsGetSymbolTests:TraderBaseInitializer
    {
        private Strategy strategy;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = this.tradingData.Get<IEnumerable<Strategy>>().Single(s => s.Id == 1);
        }

        [TestMethod]
        public void TradingDataContext_GetSymbol_returns_symbol_test()
        {
            Symbol symbol = this.tradingData.GetSymbol(this.strategy.Symbol);

            Assert.IsNotNull(symbol);
            Assert.AreEqual(symbol.Name, this.strategy.Symbol);
        }
    }
}
