﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Collections;
using ru.sazan.trader.Models;
using ru.sazan.trader.tests.Mocks;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class NamedInterfaceTests
    {
        [TestMethod]
        public void NamedCollection_test()
        {
            NamedObservableCollection<Bar> bars = new NamedObservableCollection<Bar>("Bar's collection");
            Assert.IsInstanceOfType(bars, typeof(Named));
            Assert.AreEqual("Bar's collection", bars.Name);
        }
    }
}
