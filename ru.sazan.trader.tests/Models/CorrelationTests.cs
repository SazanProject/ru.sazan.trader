﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;
using System.Globalization;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class CorrelationTests
    {
        private Strategy strategy;
        private double value;
        private DateTime date;

        [TestInitialize]
        public void Setup()
        {
            this.strategy = new Strategy(1, "Strategy", "BP12345-RF-01", "RTS-3.14_FT", 10);
            this.value = 0.98;
            this.date = DateTime.Now;
        }

        [TestMethod]
        public void Correlation_constructor_test()
        {
            Correlation correlation = new Correlation(strategy, date, value);
            Assert.AreEqual(strategy, correlation.Strategy);
            Assert.AreEqual(strategy.Id, correlation.StrategyId);
            Assert.AreEqual(date, correlation.DateTime);
            Assert.AreEqual(value, correlation.Value);
        }

        [TestMethod]
        public void Correlation_ToString_test()
        {
            Correlation correlation = new Correlation(strategy, date, value);

            CultureInfo ci = CultureInfo.InvariantCulture;

            string expect = String.Format("Correlation: {0}, {1}, {2}", correlation.StrategyId, 
                correlation.DateTime.ToString(ci), 
                correlation.Value.ToString("0.0000", ci));

            Assert.AreEqual(expect, correlation.ToString());
        }
    }
}
