﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class StopPointsSettingsTests
    {
        [TestMethod]
        public void StopLossSettings_constructor_test()
        {
            Strategy strategy = new Strategy(1, "Strategy 1", "BP12345-RF-01", "RTS-9.13_FT", 10);
            StopPointsSettings stopLossSettings = new StopPointsSettings(strategy, 300, true);

            Assert.IsTrue(stopLossSettings is Identified);
            Assert.IsInstanceOfType(stopLossSettings, typeof(PointsSettings));
            Assert.AreEqual(strategy.Id, stopLossSettings.Id);
            Assert.AreEqual(strategy, stopLossSettings.Strategy);
            Assert.AreEqual(strategy.Id, stopLossSettings.StrategyId);
            Assert.AreEqual(300, stopLossSettings.Points);
            Assert.IsInstanceOfType(stopLossSettings.Points, typeof(double));
            Assert.IsTrue(stopLossSettings.Trail);
        }
    }
}
