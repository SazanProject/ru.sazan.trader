﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ru.sazan.trader.Models;

namespace ru.sazan.trader.tests.Models
{
    [TestClass]
    public class BarSettingsTests
    {
        [TestMethod]
        public void BarSettings_Constructor_test()
        {
            Strategy strategy = new Strategy(1, "Break out strategy", "BP12345-RF-01", "RTS-9.13_FT", 10);

            BarSettings barSettings = new BarSettings(strategy, "RTS-9.13_FT", 60, 15);

            Assert.IsTrue(barSettings.Id > 0);
            Assert.IsTrue(barSettings is Identified);
            Assert.AreEqual("RTS-9.13_FT", barSettings.Symbol);
            Assert.AreEqual(60, barSettings.Interval);
            Assert.IsInstanceOfType(barSettings.Interval, typeof(int));
            Assert.AreEqual(15, barSettings.Period);
            Assert.IsInstanceOfType(barSettings.Period, typeof(int));
            Assert.AreEqual(strategy.Id, barSettings.StrategyId);
            Assert.AreSame(strategy, barSettings.Strategy);
        }
    }
}
